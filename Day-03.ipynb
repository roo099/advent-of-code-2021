{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# --- Day 3: Binary Diagnostic ---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### --- Part One ---\n",
    "\n",
    "The submarine has been making some odd creaking noises, so you ask it to produce a diagnostic report just in case.\n",
    "\n",
    "The diagnostic report (your puzzle input) consists of a list of binary numbers which, when decoded properly, can tell you many useful things about the conditions of the submarine. The first parameter to check is the power consumption.\n",
    "\n",
    "You need to use the binary numbers in the diagnostic report to generate two new binary numbers (called the gamma rate and the epsilon rate). The power consumption can then be found by multiplying the gamma rate by the epsilon rate.\n",
    "\n",
    "Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:\n",
    "\n",
    "```\n",
    "00100\n",
    "11110\n",
    "10110\n",
    "10111\n",
    "10101\n",
    "01111\n",
    "00111\n",
    "11100\n",
    "10000\n",
    "11001\n",
    "00010\n",
    "01010\n",
    "```\n",
    "\n",
    "Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.\n",
    "\n",
    "The most common second bit of the numbers in the diagnostic report is 0, so the second bit of the gamma rate is 0.\n",
    "\n",
    "The most common value of the third, fourth, and fifth bits are 1, 1, and 0, respectively, and so the final three bits of the gamma rate are 110.\n",
    "\n",
    "So, the gamma rate is the binary number 10110, or 22 in decimal.\n",
    "\n",
    "The epsilon rate is calculated in a similar way; rather than use the most common bit, the least common bit from each position is used. So, the epsilon rate is 01001, or 9 in decimal. Multiplying the gamma rate (22) by the epsilon rate (9) produces the power consumption, 198.\n",
    "\n",
    "Use the binary numbers in your diagnostic report to calculate the gamma rate and epsilon rate, then multiply them together. What is the power consumption of the submarine? (Be sure to represent your answer in decimal, not binary.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"input/day_03.txt\") as f:\n",
    "    diagnostic_report = [x.rstrip('\\n') for x in f.readlines()]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_data = [\"00100\",\"11110\",\"10110\",\"10111\",\"10101\",\"01111\",\"00111\",\"11100\",\"10000\",\"11001\",\"00010\",\"01010\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 122,
   "metadata": {},
   "outputs": [],
   "source": [
    "def common_bit(data, bit_position):\n",
    "    bits = [i[bit_position] for i in data]\n",
    "    max_bit = max(bits,key=bits.count)\n",
    "    min_bit = min(bits,key=bits.count)\n",
    "    return max_bit, min_bit\n",
    "\n",
    "def power_consumption(data_set):\n",
    "    string_length = len(data_set[0])\n",
    "    bit_result = [common_bit(data_set, i) for i in range(0,string_length)]\n",
    "    gamma = int(\"\".join([x[0] for x in bit_result]),2)\n",
    "    epsilon = int(\"\".join([x[1] for x in bit_result]),2)\n",
    "    \n",
    "    return gamma*epsilon"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 123,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3633500"
      ]
     },
     "execution_count": 123,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "power_consumption(diagnostic_report)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### --- Part Two ---\n",
    "Next, you should verify the life support rating, which can be determined by multiplying the oxygen generator rating by the CO2 scrubber rating.\n",
    "\n",
    "Both the oxygen generator rating and the CO2 scrubber rating are values that can be found in your diagnostic report - finding them is the tricky part. Both values are located using a similar process that involves filtering out values until only one remains. Before searching for either rating value, start with the full list of binary numbers from your diagnostic report and consider just the first bit of those numbers. Then:\n",
    "\n",
    "- Keep only numbers selected by the bit criteria for the type of rating value for which you are searching. Discard numbers which do not match the bit criteria.\n",
    "- If you only have one number left, stop; this is the rating value for which you are searching.\n",
    "- Otherwise, repeat the process, considering the next bit to the right.\n",
    "\n",
    "The bit criteria depends on which type of rating value you want to find:\n",
    "\n",
    "- To find oxygen generator rating, determine the most common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 1 in the position being considered.\n",
    "- To find CO2 scrubber rating, determine the least common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 0 in the position being considered.\n",
    "\n",
    "For example, to determine the oxygen generator rating value using the same example diagnostic report from above:\n",
    "\n",
    "- Start with all 12 numbers and consider only the first bit of each number. There are more 1 bits (7) than 0 bits (5), so keep only the 7 numbers with a 1 in the first position: 11110, 10110, 10111, 10101, 11100, 10000, and 11001.\n",
    "- Then, consider the second bit of the 7 remaining numbers: there are more 0 bits (4) than 1 bits (3), so keep only the 4 numbers with a 0 in the second position: 10110, 10111, 10101, and 10000.\n",
    "- In the third position, three of the four numbers have a 1, so keep those three: 10110, 10111, and 10101.\n",
    "- In the fourth position, two of the three numbers have a 1, so keep those two: 10110 and 10111.\n",
    "- In the fifth position, there are an equal number of 0 bits and 1 bits (one each). So, to find the oxygen generator rating, keep the number with a 1 in that position: 10111.\n",
    "- As there is only one number left, stop; the oxygen generator rating is 10111, or 23 in decimal.\n",
    "\n",
    "Then, to determine the CO2 scrubber rating value from the same example above:\n",
    "\n",
    "- Start again with all 12 numbers and consider only the first bit of each number. There are fewer 0 bits (5) than 1 bits (7), so keep only the 5 numbers with a 0 in the first position: 00100, 01111, 00111, 00010, and 01010.\n",
    "- Then, consider the second bit of the 5 remaining numbers: there are fewer 1 bits (2) than 0 bits (3), so keep only the 2 numbers with a 1 in the second position: 01111 and 01010.\n",
    "- In the third position, there are an equal number of 0 bits and 1 bits (one each). So, to find the CO2 scrubber rating, keep the number with a 0 in that position: 01010.\n",
    "- As there is only one number left, stop; the CO2 scrubber rating is 01010, or 10 in decimal.\n",
    "\n",
    "Finally, to find the life support rating, multiply the oxygen generator rating (23) by the CO2 scrubber rating (10) to get 230.\n",
    "\n",
    "Use the binary numbers in your diagnostic report to calculate the oxygen generator rating and CO2 scrubber rating, then multiply them together. What is the life support rating of the submarine? (Be sure to represent your answer in decimal, not binary.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 187,
   "metadata": {},
   "outputs": [],
   "source": [
    "def check_life_support(data, ls_part):\n",
    "    part = {\n",
    "        \"o2\": (\"1\", 0),\n",
    "        \"co2\": (\"0\", 1)\n",
    "    }\n",
    "    \n",
    "    for i in range(0,len(data[0])):\n",
    "        common_bits = common_bit(data, i)\n",
    "        if common_bits[0] == common_bits[1]:\n",
    "            check = part[ls_part][0]\n",
    "        else:\n",
    "            check = common_bits[part[ls_part][1]]\n",
    "        data = [x for x in data if x[i] == check]  \n",
    "        if len(data) == 1: break\n",
    "    return int(data[0],2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 192,
   "metadata": {},
   "outputs": [],
   "source": [
    "def life_support(data):\n",
    "    oxygen = check_life_support(data, \"o2\")\n",
    "    co2 = check_life_support(data, \"co2\")\n",
    "    return oxygen*co2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 195,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4550283"
      ]
     },
     "execution_count": 195,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "life_support(diagnostic_report)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
